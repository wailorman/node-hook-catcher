var express = require( 'express' );

var config = [
    // docker config filter
    {
        filter: {
            callback_url: function ( val ) {
                // registry.hub.docker.com
                return val.match( /(registry\.hub\.docker\.com)/ );
            },
            repository: {
                repo_name: 'svendowideit/testhook'
            }
        },
        exec: function ( payload ) {
            // ...
        }
    },
    {
        filter: {

        }
    }
];